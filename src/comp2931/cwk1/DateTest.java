package comp2931.cwk1;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;



public class DateTest {

    private Date birthday;

    @Before
    public void setUp() {
        birthday = new Date(1997,7, 13);
    }

    @Test
    public void equals() throws Exception {
    }

    @Test
    public void getDayOfYear(){
        assertThat(birthday.getDayOfYear(),is(194));
    }

    @Test(expected = IllegalArgumentException.class)
    public void yearTooLow() {
     new Date(-1, 1, 1);

    }

    @Test(expected = IllegalArgumentException.class)
    public void monthTooLow() {
        new Date(2000, -1, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void monthTooHigh() {
        new Date(2000, 13, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dayTooLow() {
        new Date(2000,10,-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void dayTooHigh(){
        new Date(2000,10,32);
    }

    @Test
    public void dayOfYear(){
        new Date(2000,10,31);
    }


}