// Class for COMP2931 Coursework 1

package comp2931.cwk1;


import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date {

    private int year;
    private int month;
    private int day;




    /**
     * Creates a date using the given values for year, month and day.
     *
     * @param y Year
     * @param m Month
     * @param d Day
     */
    public Date(int y, int m, int d) {
        year = y;
        month = m;
        day = d;


        set(y, m, d);

    }

    /**
     * Returns the year component of this date.
     *
     * @return Year
     */
    public int getYear() {
        return year;
    }

    /**
     * Returns the month component of this date.
     *
     * @return Month
     */
    public int getMonth() {
        return month;
    }

    /**
     * Returns the day component of this date.
     *
     * @return Day
     */
    public int getDay() {
        return day;
    }

    /**
     * Provides a string representation of this date.
     * <p>
     * ISO 8601 format is used (YYYY-MM-DD).
     *
     * @return Date as a string
     */


    public int getDayOfYear(){
        int theday = this.getDay();
        int themonth = this.getMonth();
        int theyear = this.getYear();
        int dayofyear;

        if (themonth == 1) {
            dayofyear = theday;
            return dayofyear;

        }

        if (themonth == 2) {
            dayofyear = theday + 31;
            return dayofyear;
        }

        if (themonth == 3) {
            dayofyear = theday + 31 + 28;
            return dayofyear;
        }

        if (themonth == 4) {
            dayofyear = theday + 31 + 28 + 31;
            return dayofyear;
        }

        if (themonth == 5) {
            dayofyear = theday + 31 + 28 + 31 + 30;
            return dayofyear;
        }

        if (themonth == 6) {
            dayofyear = theday + 31 + 28 + 31 + 30 + 31;
            return dayofyear;
        }

        if (themonth == 7) {
            dayofyear = theday + 31 + 28 + 31 + 30 + 31 + 30;
            return dayofyear;
        }

        if (themonth == 8) {
            dayofyear = theday + 31 + 28 + 31 + 30 + 31 + 30 + 31;
            return dayofyear;
        }

        if (themonth == 9) {
            dayofyear = theday + 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31;
            return dayofyear;
        }

        if (themonth == 10) {
            dayofyear = theday + 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30;
            return dayofyear;
        }

        if (themonth == 11) {
            dayofyear = theday + 31 + 28 + 31 + 30 + 31 + 30 +
                    31 + 31 + 30 + 31;
            return dayofyear;
        }

        if(themonth == 12) {
            dayofyear = theday + 31 + 28 + 31 + 30 + 31 + 30 +
                    31 + 31 + 30 + 31 + 30;
            return dayofyear;
    }
    return 0;
}

    @Override
    public String toString() {
        return String.format("%04d-%02d-%2d", year, month, day);
    }

    private void set(int y, int m, int d) {
        if (y <= 0 ) {
            throw new IllegalArgumentException("ERROR: Years are out of" +
                    " bounds");
        } else if (m > 12 || m < 0) {
            throw new IllegalArgumentException("ERROR: Months are out of" +
                    " bounds");
        } else if ((d < 1 || d > 30) && (m == 11 || m == 9 || m == 6 ||
                m == 4)) {
            throw new IllegalArgumentException("ERROR: Days are out of" +
                    "bounds");
        } else if ((d < 1 || d > 31) && (m == 12 || m == 10 || m == 8 ||
                m == 7 || m == 5 || m == 3 || m == 1)) {
            throw new IllegalArgumentException("ERROR: Days are out of" +
                    " bounds");

        } else if ((0 != y % 4) && (m == 2)) {
            if (d < 1 || d > 28) {
                throw new IllegalArgumentException("ERROR: Days are out of" +
                        "bounds!");
            } else if ((0 == y % 4) && (m == 2)) {
                if (d < 1 || d > 29) {
                    throw new IllegalArgumentException("ERROR: Days are out of" +
                            "bounds!");
                }
            } else {
                year = y;
                month = m;
                day = d;

            }
        }
    }


    @Override
    public boolean equals(Object other) {
        if (other == this) {

            return true;
        } else if (!(other instanceof Date)) {

            return false;
        } else {
            Date otherDate = (Date) other;
            return getYear() == otherDate.getYear()
                    && getMonth() == otherDate.getMonth()
                    && getDay() == otherDate.getDay();
        }
    }
}
